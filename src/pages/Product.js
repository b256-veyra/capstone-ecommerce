import { useState, useEffect } from 'react';
import ProductCard from '../components/ProductCard';

export default function Product() {

	// console.log(CourseData);

	// State that will be used to store the courses retrieved from the database
    const [ products, setProducts ] = useState([]);

	// Retrieves the courses from the database upon initial render of the "Courses" component
	useEffect(() => {

		fetch(`${process.env.REACT_APP_API_URL}/product/active-products`)
		.then(res => res.json())
		.then(data => {

			// console.log(data);

			setProducts(data.map(product => {
				return(
					<ProductCard key={product.id} productProp={product} />
				)
			}))
		})
	})

	return (
		<>
			<h1>Trending</h1>
			{products}
		</>
	)
}