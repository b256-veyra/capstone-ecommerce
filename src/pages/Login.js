import { useState, useEffect, useContext } from 'react';
import { Form, Button, Card } from 'react-bootstrap';
import { Navigate, Link } from 'react-router-dom';
import UserContext from "../userContext";
import Swal from 'sweetalert2';
 
export default function Login() {

		const { user, setUser } = useContext(UserContext);
		const [email, setEmail] = useState('');
		const [password, setPassword] = useState('');
		const [isActive, setIsActive] = useState(false);

		console.log(email);
		console.log(password);

		useEffect (() => {
			if(email !== '' && password !== '') {

				setIsActive(true)

			} else {

				setIsActive(false)

			}
		})

		function loginUser(e) {

			e.preventDefault();

			// Process a fetch request to the corresponding backend API
			fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
				method: "POST",
				headers: {
					'Content-Type': 'application/json'
				},
				body: JSON.stringify({
					email: email,
					password: password
				})
			})
			.then(res => res.json())
			.then(data => {

				console.log(data)

				if(typeof data.access !== "undefined") {

					localStorage.setItem('token', data.access)
					retrieveUserDetails(data.access)
				
					Swal.fire({
						title: "Login Successful",
						icon: "success",
						text: "Welcome to EasyPC!"
					})

				} else {

					Swal.fire({
						title: "Authentication failed",
						icon: "error",
						text: "Check your login details and try again!"
					})
				}

			})

			setEmail('');
			setPassword('');

		}

		const retrieveUserDetails = (token) => {


			fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
				headers: {
					Authorization: `Bearer ${token}`
				}
			})
			.then(res => res.json())
			.then(data => {
				console.log(data);
				setUser({
					id: data._id,
					isAdmin: data.isAdmin
				})
			})
		}

		return (
			(user.id !== null) ?
				<Navigate to="/" />
			:
			<Form onSubmit={e => loginUser(e)} className="d-flex justify-content-center bg-light" id="login">

			<Card className="card shadow text-center col-sm-12 col-md-8 col-lg-8 col-xl-4 mb-5 p-3">
			      <Form.Group className="mb-3" controlId="formBasicEmail">
			        <Form.Label>Email address</Form.Label>
			        <Form.Control type="email" placeholder="Enter email" value={email} onChange={e => setEmail(e.target.value)}/>
			      </Form.Group>

			      <Form.Group className="mb-3" controlId="formBasicPassword">
			        <Form.Label>Password</Form.Label>
			        <Form.Control type="password" placeholder="Password" value={password} onChange={e => setPassword(e.target.value)}/>
			      </Form.Group>

			      {
			      	isActive ?
			      		<Button variant="primary" type="submit" id="submitBtn">Submit</Button>
			      	:
			      		<Button variant="danger" type="submit" id="submitBtn" disabled>Submit</Button>
			      }
			     	<p className="text-center mt-3">
	         		No account yet? Click <Link to="/register"> here</Link> to register.
	         		</p>

		    </Card>  
		    </Form>
		)
}
  