import { useState, useEffect, useContext } from 'react';
import { Form, Button, Card } from 'react-bootstrap';
import { Navigate, useNavigate } from 'react-router-dom';
import UserContext from '../userContext';
import Swal from 'sweetalert2';

export default function Register() {

    const { user } = useContext(UserContext);

    const navigate = useNavigate();

    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [email, setEmail] = useState('');
    const [mobileNo, setMobileNo] = useState('');
    const [password1, setPassword1] = useState('');
    const [password2, setPassword2] = useState('');
    const [isActive, setIsActive] = useState(false);

    // useEffect() - whenever there is a change in our webpage
    useEffect(() => {

        // Validation to enable submit button when all fields are populated and both passwords match
        if((firstName !== '' && lastName !== '' && email !== '' && mobileNo.length === 11 && password1 !== '' && password2 !== '') && (password1 === password2)){
            setIsActive(true);
        } else {
            setIsActive(false);
        }

    }, [firstName, lastName, email, mobileNo, password1, password2]);


    const handleSubmit = (e) => {
    e.preventDefault();

    if (password1 !== password2) {
      Swal.fire({
        title: 'Passwords do not match',
        icon: 'error',
        text: 'Please make sure the passwords match.',
      });
      return;
    }

    // Perform the registration request here
    fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        firstName: firstName,
        lastName: lastName,
        email: email,
        mobileNo: mobileNo,
        password: password1,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        if (data === true) {
          setFirstName('');
          setLastName('');
          setEmail('');
          setMobileNo('');
          setPassword1('');
          setPassword2('');

          Swal.fire({
            title: 'Registration successful',
            icon: 'success',
            text: 'Welcome to EasyPC!',
          }).then(() => navigate('/login'));
        } else {
          Swal.fire({
            title: 'Something went wrong',
            icon: 'error',
            text: 'Please try again.',
          });
        }
      })
      .catch((error) => {
        console.log(error);
        Swal.fire({
          title: 'Something went wrong',
          icon: 'error',
          text: 'Please try again.',
        });
      });
  };

  function registerUser(e) {
    e.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        email: email,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        if (data === true) {
          Swal.fire({
            title: 'Email already taken',
            icon: 'error',
            text: 'Please provide a different email.',
          });
        } else {
          fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                firstName: firstName,
                lastName: lastName,
                email: email,
                mobileNo: mobileNo,
                password: password1
            }),
          })
            .then((res) => res.json())
            .then((data) => {
              console.log(data);

              if (data === true) {
                setFirstName('');
                setLastName('');
                setEmail('');
                setMobileNo('');
                setPassword1('');
                setPassword2('');

                Swal.fire({
                  title: 'Registration successful',
                  icon: 'success',
                  text: 'Welcome to EasyPC!',
                }).then(() => navigate('/login'));
              } else {
                Swal.fire({
                  title: 'Something went wrong',
                  icon: 'error',
                  text: 'Please try again.',
                });
              }
            });
        }
      });
  }


    return (
        (user.id !== null) ?
                <Navigate to="/login" />
            :
        <Form onSubmit={e => registerUser(e)} className="d-flex justify-content-center bg-light">

            <Card className="card shadow text-center col-sm-12 col-md-8 col-lg-8 col-xl-4 mb-5 p-3" id="register">
              <Form.Group className="mb-3" controlId="formBasicFirstName">
                <Form.Label>First Name</Form.Label>
                <Form.Control type="firstName" placeholder="Enter First Name" value={firstName} onChange={e => setFirstName(e.target.value)}/>
              </Form.Group>

              <Form.Group className="mb-3" controlId="formBasicLastName">
                <Form.Label>Last Name</Form.Label>
                <Form.Control type="lastName" placeholder="Enter Last Name" value={lastName} onChange={e => setLastName(e.target.value)}/>
              </Form.Group>

              <Form.Group className="mb-3" controlId="formBasicEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control type="email" placeholder="Enter email" value={email} onChange={e => setEmail(e.target.value)}/>
              </Form.Group>

              <Form.Group className="mb-3" controlId="formBasicMobileNumber">
                <Form.Label>Mobile No.</Form.Label>
                <Form.Control type="" placeholder="Enter Mobile Number" value={mobileNo} onChange={e => setMobileNo(e.target.value)}/>
              </Form.Group>

              <Form.Group className="mb-3" controlId="formBasicPassword">
                <Form.Label>Password</Form.Label>
                <Form.Control type="password" placeholder="Password" value={password1} onChange={e => setPassword1(e.target.value)}/>
              </Form.Group>

              <Form.Group className="mb-3" controlId="formBasicPassword2">
                <Form.Label>Verify Password</Form.Label>
                <Form.Control type="password" placeholder="Verify Password" value={password2} onChange={e => setPassword2(e.target.value)}/>
              </Form.Group>
              
              {
                isActive ?
                    <Button variant="primary" type="submit" id="submitBtn">Submit</Button>
                :
                    <Button variant="danger" type="submit" id="submitBtn" disabled>Submit</Button>
              }
            </Card>
        </Form>
    )
}

