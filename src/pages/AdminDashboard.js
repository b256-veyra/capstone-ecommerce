import { useState, useEffect, useContext } from 'react';
import UserContext from '../userContext';
import { Link, useNavigate } from 'react-router-dom';
import { Button, ButtonGroup, Row, Col, Modal, Form, Table, Container, Image } from 'react-bootstrap';
import { RiEyeLine, RiEditLine, RiArrowGoBackLine, RiCheckLine, RiCloseLine } from 'react-icons/ri';

import Swal from 'sweetalert2';

export default function AdminDashboard() {
  const navigate = useNavigate();
  const { user } = useContext(UserContext);
  const [name, setName] = useState('');
  const [productId, setProductId] = useState('');
  const [description, setDescription] = useState('');
  const [price, setPrice] = useState('');
  const [image, setImage] = useState('');
  const [isActive, setIsActive] = useState(false);
  const [allProducts, setAllProducts] = useState([]);
  const [editShow, setEditShow] = useState(false);

  useEffect(() => {
    if (name !== '' && description !== '' && price !== '') {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [name, description, price, image]);

  // Show AllProducts
  const fetchAllProducts = () => {
    fetch(`${process.env.REACT_APP_API_URL}/product/all`)
      .then(res => res.json())
      .then(data => {
        setAllProducts(data);
      });
  };

  useEffect(() => {
    fetchAllProducts();
  }, []);

  // End Show AllProducts

  // CreateProduct Modal
  const [addShow, setAddShow] = useState(false);

  const addHandleClose = () => setAddShow(false);
  const addHandleShow = () => setAddShow(true);

  // End of CREATE PRODUCT Modal

  function createProduct(e) {
    e.preventDefault();

    const productData = {
      name: name,
      description: description,
      price: parseFloat(price),
      image: image,
    };

    fetch(`${process.env.REACT_APP_API_URL}/product/create`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
      body: JSON.stringify(productData),
    })
      .then((res) => res.json())
      .then((data) => {
        if (data) {
          Swal.fire({
            title: 'You have successfully added a new product!',
            icon: 'success',
            text: 'Product list has been updated',
          });

          setName('');
          setDescription('');
          setPrice('');
          setImage('');
          setAddShow(false);
          fetchAllProducts();
        } else {
          Swal.fire({
            title: 'Unexpected Error found!',
            icon: 'success',
            text: 'Please try again!',
          });
        }
      });
  }

  const archiveProduct = (productId) => {
    fetch(`${process.env.REACT_APP_API_URL}/product/archive/${productId}`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
      body: JSON.stringify({
        isActive: false,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        if (data) {
          Swal.fire({
            title: 'Product is successfully archived!',
            icon: 'success',
            text: 'The product is now in the archive folder',
          });

          fetchAllProducts();
        } else {
          Swal.fire({
            title: 'Something went wrong',
            icon: 'error',
            text: 'Please try again later!',
          });
        }
      });
  };

  const reactivateProduct = (productId) => {
    fetch(`${process.env.REACT_APP_API_URL}/product/reactivate/${productId}`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
      body: JSON.stringify({
        isActive: true,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        if (data) {
          Swal.fire({
            title: 'Product is successfully reactivated!',
            icon: 'success',
            text: 'The product is now up and running',
          });

          fetchAllProducts();
        } else {
          Swal.fire({
            title: 'Something went wrong',
            icon: 'error',
            text: 'Please try again later!',
          });
        }
      });
  };

  const openEditModal = (productId) => {
    const product = allProducts.find((p) => p._id === productId);
    if (product) {
      setProductId(product._id);
      setName(product.name);
      setDescription(product.description);
      setPrice(product.price);
      setImage(product.image);
      setEditShow(true);
    }
  };

  const closeEditModal = () => {
    setEditShow(false);
    setProductId('');
    setName('');
    setDescription('');
    setPrice('');
    setImage('');
  };

  const updateProduct = (e) => {
    e.preventDefault();

    const updatedProduct = {
      name: name,
      description: description,
      price: parseFloat(price),
      image: image,
    };

    fetch(`${process.env.REACT_APP_API_URL}/product/update/${productId}`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
      body: JSON.stringify(updatedProduct),
    })
      .then((res) => res.json())
      .then((data) => {
        if (data) {
          Swal.fire({
            title: 'Product updated successfully!',
            icon: 'success',
            text: 'Product details have been updated',
          });

          closeEditModal();
          fetchAllProducts();
        } else {
          Swal.fire({
            title: 'Unexpected Error found!',
            icon: 'success',
            text: 'Please try again!',
          });
        }
      });
  };

  return (
    <>
      {user.isAdmin !== true ? (
        navigate('/UnauthorizedAccess')
      ) : (
        <>
          <div>
            <Row className="text-center m-3">
              <Col>
                <h1>Admin Dashboard</h1>            
              </Col>
            </Row>
          </div>
          <div>
            <Row className="text-center m-2">
              <Col>
                <Table striped bordered hover>
                  <thead>
                    <tr>
                      <th>Product Name</th>
                      <th>Image</th>
                      <th>Description</th>
                      <th>Price</th>
                      <th>Available</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    {allProducts.map((product) => (
                      <tr key={product._id}>
                        <td>{product.name}</td>
                        <td>
                          <Image src={product.image} style={{ width: '6rem' }} />
                        </td>
                        <td>{product.description}</td>
                        <td>{product.price}</td>
                        <td>{product.isActive ? "Yes" : "No"}</td>
                        <td>
                          <Button variant="info" onClick={() => openEditModal(product._id)}>
                            <RiEditLine /> Edit
                          </Button>{" "}
                          <ButtonGroup>
                            {product.isActive ? (
                              <>
                                <Button variant="primary" onClick={() => archiveProduct(product._id)}>
                                  <RiCloseLine /> Deactivate
                                </Button>
                                <Button variant="outline-primary" disabled>
                                  <RiCheckLine /> Activate
                                </Button>
                              </>
                            ) : (
                              <>
                                <Button variant="outline-primary" disabled>
                                  <RiCloseLine /> Deactivate
                                </Button>
                                <Button variant="primary" onClick={() => reactivateProduct(product._id)}>
                                  <RiCheckLine /> Activate
                                </Button>
                              </>
                            )}
                          </ButtonGroup>
                        </td>
                      </tr>
                    ))}
                  </tbody>
                </Table>
              </Col>
            </Row>
          </div>

          {/* ADD PRODUCT MODAL */}
          <Modal show={addShow} onHide={addHandleClose}>
            <Modal.Header closeButton>
              <Modal.Title>Add New Product</Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <Container fluid>
                <Form onSubmit={(e) => createProduct(e)}>
                  <Form.Group className="mb-3" controlId="nameProduct">
                    <Form.Label>Product Name</Form.Label>
                    <Form.Control
                      type="text"
                      placeholder="Enter product name"
                      value={name}
                      onChange={(e) => setName(e.target.value)}
                    />
                    <Form.Text className="text-muted"></Form.Text>
                  </Form.Group>

                  <Form.Group className="mb-3" controlId="descriptionProduct">
                    <Form.Label>Description</Form.Label>
                    <Form.Control
                      style={{ height: '12rem' }}
                      as="textarea"
                      placeholder="Enter product description"
                      value={description}
                      onChange={(e) => setDescription(e.target.value)}
                    />
                    <Form.Text className="text-muted"></Form.Text>
                  </Form.Group>

                  <Form.Group className="mb-3" controlId="priceProduct">
                    <Form.Label>Price</Form.Label>
                    <Form.Control
                      type="text"
                      placeholder="Enter product price"
                      value={price}
                      onChange={(e) => setPrice(e.target.value)}
                    />
                  </Form.Group>
                  <Form.Group className="mb-3" controlId="imageProduct">
                    <Form.Label>Image Url</Form.Label>
                    <Form.Control
                      type="text"
                      placeholder="Enter image url"
                      value={image}
                      onChange={(e) => setImage(e.target.value)}
                    />
                  </Form.Group>

                  {isActive ? (
                    <Button variant="primary" type="submit" id="submitBtn">
                      Submit
                    </Button>
                  ) : (
                    <Button variant="danger" type="submit" id="submitBtn" disabled>
                      Submit
                    </Button>
                  )}
                </Form>
              </Container>
            </Modal.Body>
            <Modal.Footer>
              <Button variant="secondary" onClick={addHandleClose} as={Link} to={'/adminDashboard'}>
                Close
              </Button>
            </Modal.Footer>
          </Modal>
          {/* END of ADD PRODUCT MODAL */}

          {/* EDIT PRODUCT MODAL */}
          <Modal show={editShow} onHide={closeEditModal}>
            <Modal.Header closeButton>
              <Modal.Title>Edit Product</Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <Container fluid>
                <Form onSubmit={updateProduct}>
                  <Form.Group className="mb-3" controlId="editNameProduct">
                    <Form.Label>Product Name</Form.Label>
                    <Form.Control
                      type="text"
                      placeholder="Enter product name"
                      value={name}
                      onChange={(e) => setName(e.target.value)}
                    />
                    <Form.Text className="text-muted"></Form.Text>
                  </Form.Group>

                  <Form.Group className="mb-3" controlId="editDescriptionProduct">
                    <Form.Label>Description</Form.Label>
                    <Form.Control
                      style={{ height: '12rem' }}
                      as="textarea"
                      placeholder="Enter product description"
                      value={description}
                      onChange={(e) => setDescription(e.target.value)}
                    />
                    <Form.Text className="text-muted"></Form.Text>
                  </Form.Group>

                  <Form.Group className="mb-3" controlId="editPriceProduct">
                    <Form.Label>Price</Form.Label>
                    <Form.Control
                      type="text"
                      placeholder="Enter product price"
                      value={price}
                      onChange={(e) => setPrice(e.target.value)}
                    />
                  </Form.Group>
                  <Form.Group className="mb-3" controlId="editImageProduct">
                    <Form.Label>Image Url</Form.Label>
                    <Form.Control
                      type="text"
                      placeholder="Enter image url"
                      value={image}
                      onChange={(e) => setImage(e.target.value)}
                    />
                  </Form.Group>

                  {isActive ? (
                    <Button variant="primary" type="submit" id="editSubmitBtn">
                      Update
                    </Button>
                  ) : (
                    <Button variant="danger" type="submit" id="editSubmitBtn" disabled>
                      Update
                    </Button>
                  )}
                </Form>
              </Container>
            </Modal.Body>
            <Modal.Footer>
              <Button variant="secondary" onClick={closeEditModal}>
                Close
              </Button>
            </Modal.Footer>
          </Modal>
          <div>
            <Row className="text-center m-3">
              <Col>
                <Button variant="primary" onClick={addHandleShow}>
                  Add New Product
                </Button>{" "}
              
              </Col>
            </Row>
          </div>
    
        </>
      )}
    </>
  );
}
