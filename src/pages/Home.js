import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
import Product from '../pages/Product';
import { useState, useEffect } from 'react';


export default function Home() {
    return (
        <>
            <Banner />
            <Product />
        </>
    )
}