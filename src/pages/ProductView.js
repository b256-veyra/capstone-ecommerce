import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import { useParams, Link, useNavigate } from 'react-router-dom';
import UserContext from '../userContext';
import Swal from 'sweetalert2';
import { BsFillCartDashFill,  BsFillCartPlusFill } from "react-icons/bs";

export default function ProductView() {

  const { user } = useContext(UserContext);
  const { productId } = useParams();
  const navigate = useNavigate();
  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [price, setPrice] = useState(0);
  const [userId, setUserId] = useState('');
  const [totalAmount, setTotalAmount] =('')
  const [isActive, setIsActive] = useState(false);


  useEffect(() => {

    console.log(productId);

    fetch(`${process.env.REACT_APP_API_URL}/product/${productId}`)
    .then(res => res.json())
    .then(data => {

      setName(data.name);
      setDescription(data.description);
      setPrice(data.price);

    })
  })

  const createOrder = (productId) => {

    
     fetch(`${process.env.REACT_APP_API_URL}/users/create-order`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}` 

      },
      body: JSON.stringify({
                    productId: productId,
                    productName: name,
                    quantity: quantity
      })
    })
    .then(res => res.json())
    .then(data => {
      
      console.log(data)
      if(data) {
        Swal.fire({
                    title: "ORDER SUCCESS!",
                    icon: "success",
                    text: "The new order was added to the order list."
                })

                setQuantity('')

      } else {
        Swal.fire({
                    title: "ORDER UNSSUCCESSFUL!",
                    icon: "error",
                    text: "Please try again later.",
                })
      }
    })
  }


  const [quantity, setQuantity] = useState(0)

  function addToCart() {

    setQuantity(quantity + 1);

  }

  function minusToCart() {

    if(quantity > 0) {
      setQuantity(quantity - 1);

    } else {

      alert("You have no product in your cart")
    }
    
  }

  useEffect(() => {

       
            if(quantity === 0){
                setIsActive(true);
            } else {
                setIsActive(false);
            }

        }, [quantity]);


  return(
    <Container className="mt-5">
      <Row>
        <Col lg={{ span: 6, offset: 3 }}>
          <Card>
            <Card.Body className="text-center">
              <Card.Title>{name}</Card.Title>
              <Card.Subtitle>Description:</Card.Subtitle>
              <Card.Text>{description}</Card.Text>
              <Card.Subtitle>Price:</Card.Subtitle>
              <Card.Text>PhP {price}</Card.Text>
              {
                    
                    (user.isAdmin || isActive) && (user.id === null )
                    ?
                    <>
                    <Button variant="primary" disabled >Buy Now</Button>
                    <div className="text-align-items">
                      <BsFillCartPlusFill as='btn' onClick={()=> addToCart()}/> <br/>
                      <Card.Text className="text-center">{quantity} </Card.Text>
                      <BsFillCartDashFill as='btn' onClick={()=> minusToCart()}/> <br/>
                    </div>
                    </>                    
                    :
                    <>
                    <Button variant="primary" onClick={()=> createOrder(productId)} >Buy Now</Button>
                    <div>
                    <BsFillCartPlusFill as='btn' onClick={()=> addToCart()}/>
                    <Card.Text className="text-center">{quantity} </Card.Text>
                    <BsFillCartDashFill as='btn' onClick={()=> minusToCart()}/>
                    </div>
                    </>
              }
            </Card.Body>    
          </Card>
        </Col>
      </Row>
    </Container>
  )
}
