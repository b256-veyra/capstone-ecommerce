// import { Button, Row, Col } from 'react-bootstrap';
// import { Link } from 'react-router-dom';
import Carousel from 'react-bootstrap/Carousel';

export default function Banner() {
	return(
		<Carousel fade>
	      <Carousel.Item>
	        <img
	          className="d-block w-100"
	          src="https://scontent.fmnl13-2.fna.fbcdn.net/v/t39.30808-6/279176533_5179729312085256_7439161680273528716_n.jpg?_nc_cat=108&ccb=1-7&_nc_sid=e3f864&_nc_eui2=AeGYonrTEQ8D9rFNIrN7XUfpybzXQ-VaSvTJvNdD5VpK9KPYYS8Y7-z7Tftax7qD1OXnMT3Ra4lN4_2BgDylFPJt&_nc_ohc=V312rJIqkooAX-F3FzP&_nc_zt=23&_nc_ht=scontent.fmnl13-2.fna&oh=00_AfA2ERN-jL7-BT6rVh3AAuJpVhLQpAyc8OHseeHcpfbFOQ&oe=6471B33C"
	          alt="First slide"
	        />
	      </Carousel.Item>
	      <Carousel.Item>
	        <img
	          className="d-block w-100"
	          src="https://scontent.fmnl13-1.fna.fbcdn.net/v/t39.30808-6/283783583_5271506672907519_6422979344992085343_n.jpg?_nc_cat=100&ccb=1-7&_nc_sid=e3f864&_nc_eui2=AeE7rsxocITIbdLW-FtdYceUxNwu0InR2GfE3C7QidHYZ9s7Vjqe1hbNl0JZPtCdbhEoC-x4CU4k0z9E84wFDtqI&_nc_ohc=rBwE8BIA7mUAX_g1LpK&_nc_oc=AQlPQOD4NfZJToq-DXYqMp07dAHvlmZQmtPkQf8Xi7VX7Q5IV3vSXV0I4t3D4aXi7V0&_nc_zt=23&_nc_ht=scontent.fmnl13-1.fna&oh=00_AfDSWXEGsABTLGw0sUkM5jysFRxJIeaVrBHTnx8zlqSokw&oe=6473848A"
	          alt="Second slide"
	        />
	      </Carousel.Item>
	      <Carousel.Item>
	        <img
	          className="d-block w-100"
	          src="https://scontent.fmnl13-1.fna.fbcdn.net/v/t39.30808-6/291526878_5352071131517739_7471083411229842278_n.jpg?_nc_cat=106&ccb=1-7&_nc_sid=e3f864&_nc_eui2=AeEzOo16JGg5ZnFuA9QDBkpaBYtzh3-TXnMFi3OHf5NeczpiXIAex4DNpnZxChNnaF7bIqsa7aLEi1i3npKIQCkr&_nc_ohc=9gooiAtVfWMAX_kHDAZ&_nc_zt=23&_nc_ht=scontent.fmnl13-1.fna&oh=00_AfDz7cMN9Zh7BdD0NhxUOfuEtUjTsFmW10x4JqvAMzDQyQ&oe=647230C5"
	          alt="Third slide"
	        />
	      </Carousel.Item>
	      <Carousel.Item>
	        <img
	          className="d-block w-100"
	          src="https://scontent.fmnl13-2.fna.fbcdn.net/v/t39.30808-6/310432831_5630097327048450_5063653341935171913_n.jpg?_nc_cat=110&ccb=1-7&_nc_sid=e3f864&_nc_eui2=AeEm4Tfri7cz7cF9Wbqrj77w79aEGbs7CErv1oQZuzsISnsIwhAiucarT5YcgV_vltYkrUYKephIEe0b1bPw-JYG&_nc_ohc=vPCCexUG_BAAX_bI_wV&_nc_zt=23&_nc_ht=scontent.fmnl13-2.fna&oh=00_AfCwJDmXCkssliWfMm8CUL8FQxOjZcWf4X3bU3Y4Vpz6jg&oe=6472BE46"
	          alt="Fourth slide"
	        />
	      </Carousel.Item>
   		</Carousel>
	)
}