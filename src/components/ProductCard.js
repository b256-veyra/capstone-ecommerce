import { Container, Card, Row, Col, Image, CardGroup, Button} from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function ProductCard({productProp}) {

    
    const { name, description, price, _id } = productProp;
 
    return (
        <Container className="my-3">
            <Row>
                <Card>
                    <Card.Body>                   
                    <Card.Title>{name}</Card.Title>
                    <Card.Subtitle>Description:</Card.Subtitle>
                    <Card.Text>{description}</Card.Text>
                    <Card.Subtitle>Price:</Card.Subtitle>
                    <Card.Text>{price}</Card.Text>
                    <Button variant="primary" as={Link} to={`/productView/${_id}`}>Details</Button>
                    </Card.Body>
                </Card>
            </Row>
        </Container>
    )
}

